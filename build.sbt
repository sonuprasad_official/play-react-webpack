import WebJs._
import JsEngineKeys._
import play.PlayImport.PlayKeys.playRunHooks
import sbt._

name := """play-react-webpack"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaCore,
  javaJdbc,
  javaJpa,
  javaWs,
  specs2 % Test,
  "com.codeborne" % "phantomjsdriver" % "1.2.1",
  "org.scalatest" %% "scalatest" % "2.1.7" % "test",
  "org.scala-lang" % "scala-reflect" % "2.11.6"
)

playRunHooks <+= baseDirectory.map(Webpack.apply)

routesGenerator := InjectedRoutesGenerator

excludeFilter in (Assets, JshintKeys.jshint) := "*.js"

watchSources ~= { (ws: Seq[File]) =>
  ws filterNot { path =>
    path.getName.endsWith(".js") || path.getName == ("build")
  }
}

pipelineStages := Seq(rjs,digest, gzip)

resolvers += "typesafe" at "http://repo.typesafe.com/typesafe/repo"
